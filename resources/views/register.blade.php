<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Register</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        
    </head>
    <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sing Up Form</h3>
    <form action='./welcome' method="POST">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>

        <label for="fname">Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label><br>
        <select id="nationality" name="nationality">
          <option value="indonesia">Indonesia</option>
          <option value="jepang">Jepang</option>
          <option value="usa">USA</option>
          <option value="inggris">Inggris</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" id="language1" name="language1" value="indonesia">
        <label for="language1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="english">
        <label for="language2"> English</label><br>
        <input type="checkbox" id="language3" name="language3" value="arabic">
        <label for="language2"> Arabic</label><br>
        <input type="checkbox" id="language4" name="language4" value="japanese">
        <label for="language2"> Japanese</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="message" style="width:250px; height:150px;"></textarea><br>
        <input type="submit" value="Sign Up">
      </form>
    </body>
</html>
